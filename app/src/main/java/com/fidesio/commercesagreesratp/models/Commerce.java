package com.fidesio.commercesagreesratp.models;

import com.fidesio.commercesagreesratp.utils.DataHelper;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by damine on 3/5/2017.
 */

public class Commerce implements Serializable{

    private String recordId;
    private String ville;
    private String libelle;
    private int codePostal;
    private double latitude;
    private double longitude;

    public Commerce() {
    }

    public Commerce(String recordId, String ville, String libelle, int codePostal, double latitude, double longitude) {
        this.recordId = recordId;
        this.ville = ville;
        this.libelle = libelle;
        this.codePostal = codePostal;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Commerce(JSONObject commerce) {
        this.recordId = commerce.optString(DataHelper.KEY_RECORD_ID);
        JSONObject fields = commerce.optJSONObject(DataHelper.KEY_FIELDS);
        this.ville = fields.optString(DataHelper.KEY_VILLE);
        this.libelle = fields.optString(DataHelper.KEY_LIBELLE);
        this.codePostal = fields.optInt(DataHelper.KEY_CODE_POSTAL);
        this.latitude = fields.optJSONArray(DataHelper.KEY_COORD_GEO).optDouble(0);
        this.longitude = fields.optJSONArray(DataHelper.KEY_COORD_GEO).optDouble(1);
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @Override
    public String toString() {
        return "Commerce{" +
                "recordId='" + recordId + '\'' +
                ", ville='" + ville + '\'' +
                ", libelle='" + libelle + '\'' +
                ", codePostal=" + codePostal +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
