package com.fidesio.commercesagreesratp.utils;

import android.app.Activity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by damine on 3/5/2017.
 */

public class VolleyUtils {

    private static VolleyUtils VOLLEY_UTILS  = null;

    private VolleyUtils(){

    }

    public static VolleyUtils getInstance(){
        if (VOLLEY_UTILS==null){
            VOLLEY_UTILS = new VolleyUtils();
        }
        return VOLLEY_UTILS;
    }


    public void get(String url, Activity activity, Response.Listener listener, Response.ErrorListener error){
        Volley.newRequestQueue(activity).add(new StringRequest(Request.Method.GET, url,listener, error));
    }
}
