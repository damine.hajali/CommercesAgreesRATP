package com.fidesio.commercesagreesratp.utils;

/**
 * Created by damine on 3/5/2017.
 */

public class DataHelper {
    public static String BASE_URL = "https://data.ratp.fr/api/";

    public static String START="{%S}";

    public static String DATA_URL = BASE_URL+"records/1.0/search/?dataset=liste-des-commerces-de-proximite-agrees-ratp" +
            "&rows=10&sort=code_postal&facet=tco_libelle&facet=jour_fermeture&facet=code_postal&start=";

    public static String DATA_URL_BY_PAGE = DATA_URL+START;

    public static String KEY_RECORD_ID = "recordid";
    public static String KEY_FIELDS = "fields";
    public static String KEY_VILLE = "ville";
    public static String KEY_LIBELLE = "tco_libelle";
    public static String KEY_CODE_POSTAL = "code_postal";

    public static String KEY_COORD_GEO = "coord_geo";
    public static String KEY_RECOORDS = "records";
}
