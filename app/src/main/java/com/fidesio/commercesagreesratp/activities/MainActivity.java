package com.fidesio.commercesagreesratp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

import com.fidesio.commercesagreesratp.R;
import com.fidesio.commercesagreesratp.adapters.CommerceAdapter;
import com.fidesio.commercesagreesratp.dao.classes.CommerceDao;
import com.fidesio.commercesagreesratp.models.Commerce;
import com.fidesio.commercesagreesratp.views.LoadMoreListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.loadmore_list)
    LoadMoreListView listview;

    List<Commerce> commerces;

    String after = null;

    CommerceAdapter adapter;

    static int COUNT = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        commerces = new ArrayList<>();
        adapter = new CommerceAdapter(this,R.layout.item_commerce,commerces);
        listview.setAdapter(adapter);

        call();
        listview.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            public void onLoadMore() {
                call();
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.putExtra("obj",commerces.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(List<Commerce> newCommerces) {
        commerces.addAll(newCommerces);
        adapter.notifyDataSetChanged();
        listview.onLoadMoreComplete();
    }

    public void call() {
        COUNT++;
        new CommerceDao(this).getAllByPage(COUNT*10);
    }
}
