package com.fidesio.commercesagreesratp.dao.classes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fidesio.commercesagreesratp.dao.interfaces.ICommerceDao;
import com.fidesio.commercesagreesratp.models.Commerce;
import com.fidesio.commercesagreesratp.utils.DataHelper;
import com.fidesio.commercesagreesratp.utils.VolleyUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.fidesio.commercesagreesratp.utils.DataHelper.KEY_RECOORDS;

/**
 * Created by damine on 3/5/2017.
 */

public class CommerceDao implements ICommerceDao {

    AppCompatActivity activity;


    public CommerceDao(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getAllByPage(int start) {
        VolleyUtils.getInstance().get(DataHelper.DATA_URL_BY_PAGE.replace(DataHelper.START,String.valueOf(start)), activity, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    JSONObject mainObject = new JSONObject(response);
                    JSONArray commercesArray = mainObject.getJSONArray(DataHelper.KEY_RECOORDS);
                    EventBus.getDefault().post(parse(commercesArray));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    public List<Commerce> parse(JSONArray array) {
        List<Commerce> commerces = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Commerce commerce = null;
            try {
                commerce = new Commerce(array.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            commerces.add(commerce);
        }
        return commerces;
    }
}
