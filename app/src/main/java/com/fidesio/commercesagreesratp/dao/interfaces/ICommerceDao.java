package com.fidesio.commercesagreesratp.dao.interfaces;

import com.fidesio.commercesagreesratp.models.Commerce;

import java.util.List;

/**
 * Created by damine on 3/5/2017.
 */

public interface ICommerceDao {

    void getAllByPage(int start);
}
