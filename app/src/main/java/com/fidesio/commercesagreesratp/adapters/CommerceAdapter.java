package com.fidesio.commercesagreesratp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fidesio.commercesagreesratp.R;
import com.fidesio.commercesagreesratp.models.Commerce;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommerceAdapter extends ArrayAdapter<Commerce> {

    Context context;
    int layoutResourceId;
    List<Commerce> commerces;

    public CommerceAdapter(Context context, int layoutResourceId, List<Commerce> commerces) {
        super(context, layoutResourceId, commerces);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.commerces = commerces;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Commerce commerce = commerces.get(position);
        holder.tvLibelle.setText(commerce.getLibelle());
        holder.tvVille.setText("" + commerce.getVille()+" ("+commerce.getCodePostal()+")");

        return row;
    }

    public class ViewHolder {
        @Bind(R.id.tv_libelle)
        TextView tvLibelle;
        @Bind(R.id.tv_ville)
        TextView tvVille;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}