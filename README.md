COMMERCE AGREES RATP
=====================
[DOC LINK](https://data.ratp.fr/explore/dataset/liste-des-commerces-de-proximite-agrees-ratp/api/?disjunctive.code_postal&sort=code_postal&rows=10)

application Android permettant de recenser les commerces de proximité agréés RATP.
-------------------------------------------------------------------------------------